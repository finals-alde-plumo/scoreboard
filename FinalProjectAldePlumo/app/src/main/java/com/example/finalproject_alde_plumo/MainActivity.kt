package com.example.finalproject_alde_plumo

import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase


class MainActivity : AppCompatActivity() {
    private var points1: Long = 0
    private var points2: Long = 0
    private val database = Firebase.database("https://finals-alde-plumo-default-rtdb.asia-southeast1.firebasedatabase.app/")
    private val databaseRef1 = database.getReference("Team1")
    private val databaseRef2 = database.getReference("Team2")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setContentView(R.layout.courtcounter)

        databaseRef1.get().addOnSuccessListener {
            points1 = it.value as Long
            displayPoints1(points1)
        }
        databaseRef2.get().addOnSuccessListener {
            points2 = it.value as Long
            displayPoints2(points2)
        }
    }


    private fun displayPoints1(number: Long) {
        val points1 = findViewById<TextView>(R.id.txtPoints1)
        points1.text = "" + number
    }

    private fun displayPoints2(number: Long) {
        val points2 = findViewById<TextView>(R.id.txtPoints2)
        points2.text = "" + number
    }

    fun thpts1(view: View) {
        points1 += 3
        displayPoints1(points1)
    }

    fun thpts2(view: View) {
        points2 += 3
        displayPoints2(points2)
    }

    fun twpts1(view: View) {
        points1 += 2
        displayPoints1(points1)
    }

    fun twpts2(view: View) {
        points2 += 2
        displayPoints2(points2)
    }

    fun ftr1(view: View) {
        points1++
        displayPoints1(points1)
    }

    fun ftr2(view: View) {
        points2++
        displayPoints2(points2)
    }

    fun reset(view: View) {
        points1 = 0
        points2 = 0
        displayPoints1(points1)
        displayPoints2(points2)
    }
    fun submit(view: View) {
        databaseRef1.setValue(points1)
        databaseRef2.setValue(points2)

        val toast = Toast.makeText(applicationContext, "Uploaded to database", 5)
        toast.show()
    }
}